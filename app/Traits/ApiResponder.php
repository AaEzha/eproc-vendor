<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponder
{
    public function ok($data = null, $message = 'Success', $code = Response::HTTP_OK)
    {
        return response()->json([
            'message' => $message,
            'data' => $data,
        ], $code)->header('Content-Type', 'application/json');
    }

    public function error($message = 'Error', $code = Response::HTTP_BAD_REQUEST, $data = null)
    {
        return response()->json([
            'message' => $message,
            'data' => $data,
        ], $code)->header('Content-Type', 'application/json');
    }
}
