<?php

namespace App\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

trait FailedValidation
{
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'message'   => 'Validation errors',
            'data'      => null,
            'errors'    => $validator->errors(),
        ], Response::HTTP_UNPROCESSABLE_ENTITY)->header('Content-Type', 'application/json'));
    }
}
