<?php

namespace App\Enums;

enum VendorStatus: string
{
    case Registered = 'Registered';
    case Verified = 'Verified';
    case Approved = 'Approved';
    case NotVerified = 'Not Verified';
    case Rejected = 'Rejected';
}
