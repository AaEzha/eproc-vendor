<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class VendorBusinessPermit extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'vendor_id', 'type', 'permit_number', 'start_date', 'end_date', 'licensing_agency', 'vendor_business_field_id',
        'created_by', 'updated_by', 'deleted_by',
    ];

    protected $casts = [
        'start_date' => 'date',
        'end_date' => 'date',
    ];

    protected $attributes = [
        'created_by' => 'Sistem',
        'updated_by' => 'Sistem',
    ];

    protected $with = [
        'vendor_business_field'
    ];

    // protected function duration(): Attribute
    // {
    //     return Attribute::make(
    //         get: function (string $value, array $attributes) {
    //             // Tanggal awal dan akhir
    //             $startDate = Carbon::parse($attributes['start_date']);
    //             $endDate = Carbon::parse($attributes['end_date']);

    //             // Menghitung perbedaan
    //             $interval = $startDate->diff($endDate);

    //             $years = $interval->y;
    //             $months = $interval->m;

    //             return "$years tahun dan $months bulan";
    //         },
    //     )->withoutObjectCaching();
    // }

    public function getDurationAttribute(): string
    {
        // Tanggal awal dan akhir
        $startDate = Carbon::parse($this->start_date);
        $endDate = Carbon::parse($this->end_date);

        // Menghitung perbedaan
        $interval = $startDate->diff($endDate);

        $years = $interval->y;
        $months = $interval->m;

        return "$years tahun dan $months bulan";
    }

    public function vendor_business_field(): BelongsTo
    {
        return $this->belongsTo(VendorBusinessField::class);
    }
}
