<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorAttachment extends Model
{
    use HasFactory, SoftDeletes;

    const ATTACHMENT_CATEGORIES = [
        'Profil Perusahaan',
        'Izin Usaha',
        'Landasan Hukum',
        'Pengurus Perusahaan',
        'SPT Tahunan',
        'Tenaga Ahli',
        'Pengalaman'
    ];

    protected $fillable = [
        'name', 'category', 'expiration_date', 'document', 'document_path_raw',
        'created_by', 'updated_by', 'deleted_by', 'is_verified'
    ];

    protected $hidden = [
        'document_path_raw',
        'created_by', 'updated_by', 'deleted_by',
        'created_at', 'updated_at', 'deleted_at',
    ];

    protected $casts = [
        'expiration_date' => 'date',
        'is_verified' => 'boolean',
        'verified_at' => 'datetime'
    ];

    protected $attributes = [
        'is_verified' => false,
    ];

    public function vendor(): BelongsTo
    {
        return $this->belongsTo(Vendor::class);
    }
}
