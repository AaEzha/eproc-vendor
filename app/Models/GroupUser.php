<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupUser extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'code', 'group_name', 'keterangan',
        'created_by', 'updated_by', 'deleted_by',
    ];
}
