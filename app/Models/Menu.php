<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'menu_id';

    protected $fillable = [
        'parent_id', 'menu_seq', 'menu_name', 'level_menu', 'keterangan', 'urlto',
        'created_by', 'updated_by', 'deleted_by',
    ];

    protected $hidden = [
        'created_by', 'updated_by', 'deleted_by',
        'created_at', 'updated_at', 'deleted_at',
    ];

    protected $casts = [
        'level_menu' => 'integer',
    ];

    public function parent_menu(): BelongsTo
    {
        return $this->belongsTo(Menu::class, 'parent_id', 'menu_id');
    }

    /**
     * Get all of the children_menu for the Menu
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children_menu(): HasMany
    {
        return $this->hasMany(Menu::class, 'parent_id', 'menu_id');
    }
}
