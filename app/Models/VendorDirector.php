<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorDirector extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'vendor_director';

    protected $fillable = [
        'vendor_id', 'position_id', 'name', 'identity_no', 'npwp_no',
        'created_by', 'updated_by', 'deleted_by',
    ];

    protected $hidden = [
        'created_by', 'updated_by', 'deleted_by',
        'created_at', 'updated_at', 'deleted_at',
    ];

    protected $with = [
        'vendor_position'
    ];

    public function vendor(): BelongsTo
    {
        return $this->belongsTo(Vendor::class);
    }

    public function vendor_position(): BelongsTo
    {
        return $this->belongsTo(VendorPosition::class, 'position_id');
    }
}
