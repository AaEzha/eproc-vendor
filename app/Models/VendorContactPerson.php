<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class VendorContactPerson extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'vendor_id', 'contact_name', 'contact_email', 'contact_npwp', 'contact_identity_no', 'position_id', 'contact_phone',
        'created_by', 'updated_by', 'deleted_by'
    ];

    protected $hidden = [
        'created_by', 'updated_by', 'deleted_by',
        'created_at', 'updated_at', 'deleted_at',
    ];

    protected $with = [
        'vendor_position'
    ];

    public function vendor(): BelongsTo
    {
        return $this->belongsTo(Vendor::class);
    }

    public function vendor_position(): BelongsTo
    {
        return $this->belongsTo(VendorPosition::class, 'position_id');
    }
}
