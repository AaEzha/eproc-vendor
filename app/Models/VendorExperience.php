<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorExperience extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'vendor_id', 'business_field_id', 'job_name', 'location',
        'created_by', 'updated_by', 'deleted_by',
    ];

    protected $hidden = [
        'created_by', 'updated_by', 'deleted_by',
        'created_at', 'updated_at', 'deleted_at',
    ];

    protected $with = [
        'vendor_business_field'
    ];

    public function vendor(): BelongsTo
    {
        return $this->belongsTo(Vendor::class);
    }

    public function vendor_business_field(): BelongsTo
    {
        return $this->belongsTo(VendorBusinessField::class, 'business_field_id');
    }
}
