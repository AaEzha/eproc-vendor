<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupMenu extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'group_users_id', 'menu_id',
        'mview', 'madd', 'medit', 'mdelete', 'mapprove', 'mignore', 'mexport',
        'created_by', 'updated_by', 'deleted_by',
    ];

    protected $casts = [
        'mview' => 'boolean',
        'madd' => 'boolean',
        'medit' => 'boolean',
        'mdelete' => 'boolean',
        'mapprove' => 'boolean',
        'mignore' => 'boolean',
        'mexport' => 'boolean',
    ];

    protected $attributes = [
        'mview' => 'false',
        'madd' => 'false',
        'medit' => 'false',
        'mdelete' => 'false',
        'mapprove' => 'false',
        'mignore' => 'false',
        'mexport' => 'false',
    ];

    public function menu(): BelongsTo
    {
        return $this->belongsTo(Menu::class, 'menu_id', 'menu_id');
    }

    /**
     * Get the group_user that owns the GroupMenu
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group_user(): BelongsTo
    {
        return $this->belongsTo(GroupUser::class, 'group_users_id', 'id');
    }
}
