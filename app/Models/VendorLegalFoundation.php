<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorLegalFoundation extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'vendor_legal_foundation';

    protected $fillable = [
        'vendor_id', 'document_no', 'document_date', 'notaris_name',
        'created_by', 'updated_by', 'deleted_by',
    ];

    protected $hidden = [
        'created_by', 'updated_by', 'deleted_by',
        'created_at', 'updated_at', 'deleted_at',
    ];

    protected $casts = [
        'document_date' => 'date'
    ];
}
