<?php

namespace App\Models;

use App\Enums\VendorStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Vendor extends Model
{
    use HasFactory, SoftDeletes;

    const CREATED_AT = 'registered_at';

    protected $fillable = [
        'vendor_number', 'username', 'password', 'company_name', 'company_npwp', 'company_phone_number', 'company_email', 'company_address',
        'company_fax', 'city_id', 'postal_code', 'vendor_type',
        'updated_by', 'deleted_by',
    ];

    protected $casts = [
        'is_active' => 'boolean',
        'status' => VendorStatus::class,
        'city_id' => 'integer',
        'verification_date' => 'timestamp',
        'password' => 'hashed',
    ];

    protected $hidden = [
        'verification_date', 'verificator_id', 'registered_at', 'password',
        'updated_by', 'deleted_by',
        'updated_at', 'deleted_at',
    ];

    protected $attributes = [
        'status' => VendorStatus::Registered,
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'user_name', 'username');
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function contact_people(): HasMany
    {
        return $this->hasMany(VendorContactPerson::class);
    }

    public function vendor_bank_accounts(): HasMany
    {
        return $this->hasMany(VendorBankAccount::class);
    }

    public function vendor_business_permits(): HasMany
    {
        return $this->hasMany(VendorBusinessPermit::class);
    }

    public function vendor_legal_foundations(): HasMany
    {
        return $this->hasMany(VendorLegalFoundation::class);
    }

    public function vendor_directors(): HasMany
    {
        return $this->hasMany(VendorDirector::class);
    }

    public function vendor_annual_spts(): HasMany
    {
        return $this->hasMany(VendorAnnualSpt::class);
    }

    public function vendor_experts(): HasMany
    {
        return $this->hasMany(VendorExpert::class);
    }

    public function vendor_experiences(): HasMany
    {
        return $this->hasMany(VendorExperience::class);
    }

    public function vendor_attachments(): HasMany
    {
        return $this->hasMany(VendorAttachment::class);
    }
}
