<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;

    protected $table = 'app_banks';
    public $timestamps = false;

    protected $fillable = [
        'bank_name', 'bank_logo'
    ];

    protected $hidden = [
        'bank_logo'
    ];
}
