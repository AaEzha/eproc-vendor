<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
        'password',
        'user_name',
        'id_card',
        'firstname',
        'lastname',
        'sex',
        'department',
        'company',
        'phone',
        'image',
        'province',
        'city',
        'address',
        'status',
        'create_by',
        'modified_by',
        'deleted_by',
        'deleted_at',
        'group_users_id',
        'npwp'

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_by', 'updated_by', 'deleted_by',
        'created_at', 'updated_at', 'deleted_at',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }

    public function getFullnameAttribute()
    {
        return $this->firstname ? ucfirst($this->firstname) . ' ' . ucfirst($this->lastname) : $this->user_name;
    }

    public function vendor(): HasOne
    {
        return $this->hasOne(Vendor::class, 'username', 'user_name');
    }

    public function group_user(): HasOne
    {
        return $this->hasOne(GroupUser::class, 'id', 'group_users_id');
    }
}
