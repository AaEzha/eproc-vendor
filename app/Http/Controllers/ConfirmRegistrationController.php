<?php

namespace App\Http\Controllers;

use App\Enums\VendorStatus;
use App\Models\Vendor;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ConfirmRegistrationController extends Controller
{
    use ApiResponder;

    /**
     * Handle the incoming request.
     */
    public function __invoke($vendor_number, Vendor $vendor, $random_code)
    {
        DB::beginTransaction();
        try {
            $cek = md5($vendor->vendor_number . "|" . $vendor->registered_at);
            if ($cek !== $vendor_number) throw new \Exception('Kode tidak dikenal');

            $vendor->is_active = true;
            $vendor->status = VendorStatus::NotVerified;
            $vendor->updated_by = Crypt::decryptString($random_code);
            $vendor->save();

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(
            message: 'Vendor Anda berhasil terkonfirmasi. Silahkan lanjutkan untuk melengkapi data.',
            data: 'url-login nanti di sini'
        );
    }
}
