<?php

namespace App\Http\Controllers\API\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\VendorLegalFoundationStore;
use App\Http\Requests\API\Vendor\VendorLegalFoundationUpdate;
use App\Models\User;
use App\Models\VendorLegalFoundation;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VendorLegalFoundationController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $user = User::find(auth()->id());
            $legal_foundations = $user->vendor->vendor_legal_foundations;
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($legal_foundations);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VendorLegalFoundationStore $request)
    {
        try {
            DB::beginTransaction();
            $user = User::find(auth()->id());
            $legal_foundations = $user->vendor->vendor_legal_foundations()->create($request->all());
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($legal_foundations);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $legal_foundation = VendorLegalFoundation::find($id);
            if (!$legal_foundation) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($legal_foundation->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($legal_foundation);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VendorLegalFoundationUpdate $request, string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $legal_foundation = VendorLegalFoundation::find($id);
            if (!$legal_foundation) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($legal_foundation->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $legal_foundation->update($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($legal_foundation);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $legal_foundation = VendorLegalFoundation::find($id);
            if (!$legal_foundation) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($legal_foundation->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $legal_foundation->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
