<?php

namespace App\Http\Controllers\API\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\VendorDirectorStore;
use App\Http\Requests\API\Vendor\VendorDirectorUpdate;
use App\Models\User;
use App\Models\VendorDirector;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VendorDirectorController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $user = User::find(auth()->id());
            $directors = $user->vendor->vendor_directors;
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($directors);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VendorDirectorStore $request)
    {
        try {
            DB::beginTransaction();
            $user = User::find(auth()->id());
            $directors = $user->vendor->vendor_directors()->create($request->validated());
            $directors->load('vendor_position');
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($directors);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $director = VendorDirector::find($id);
            if (!$director) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($director->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $director->load('vendor_position');
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($director);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VendorDirectorUpdate $request, string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $director = VendorDirector::find($id);
            if (!$director) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($director->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $director->update($request->validated());

            $director->load('vendor_position');
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($director);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $director = VendorDirector::find($id);
            if (!$director) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($director->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $director->update([
                'deleted_by' => auth()->id() ?? null,
            ]);
            $director->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
