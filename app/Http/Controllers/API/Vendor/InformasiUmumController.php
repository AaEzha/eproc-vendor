<?php

namespace App\Http\Controllers\API\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\InformasiUmumStore;
use App\Models\City;
use App\Models\User;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InformasiUmumController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;
            $vendor->load('city');
            $city = City::find($vendor->city_id);
            $vendor['province_id'] = $city->province_id;
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($user->vendor);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(InformasiUmumStore $request)
    {
        try {
            DB::beginTransaction();
            $user = User::find(auth()->id());
            $user->vendor->update($request->validated());
            $vendor = $user->vendor;
            $vendor->load('city');
            $city = City::find($vendor->city_id);
            $vendor['province_id'] = $city->province_id;
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($vendor);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return $this->error();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        return $this->error();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        return $this->error();
    }
}
