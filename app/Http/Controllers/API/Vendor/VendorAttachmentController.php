<?php

namespace App\Http\Controllers\API\Vendor;

use App\Enums\VendorStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\VendorAttachmentStore;
use App\Http\Requests\API\Vendor\VendorAttachmentUpdate;
use App\Models\User;
use App\Models\VendorAttachment;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class VendorAttachmentController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $user = User::find(auth()->id());
            $attachments = $user->vendor->vendor_attachments;
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($attachments);
    }

    public function create()
    {
        return $this->ok(VendorAttachment::ATTACHMENT_CATEGORIES);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VendorAttachmentStore $request)
    {
        try {
            DB::beginTransaction();
            $file = $request->file('document');
            $document = $file->getClientOriginalName();
            $document_path_raw = $request->file('document')->store('vendor-documents');
            $data = array_merge($request->validated(), compact('document', 'document_path_raw'));

            $user = User::find(auth()->id());
            $attachment = $user->vendor->vendor_attachments()->create($data);
            $attachment->document_path = Storage::url($document_path_raw);
            $attachment->document_type = Storage::mimeType($document_path_raw);
            $attachment->save();

            $user->vendor->status = VendorStatus::NotVerified;
            $user->vendor->save();
            DB::commit();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($attachment);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $attachment = VendorAttachment::find($id);
            if (!$attachment) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($attachment->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($attachment);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VendorAttachmentUpdate $request, string $id)
    {
        return $this->error();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            DB::beginTransaction();
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $attachment = VendorAttachment::find($id);
            if (!$attachment) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($attachment->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $attachment->update([
                'deleted_by' => auth()->id() ?? null,
            ]);

            if (Storage::disk(config('filesystems.default'))->exists($attachment->document_path_raw)) {
                Storage::delete($attachment->document_path_raw);
            }
            $attachment->delete();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
