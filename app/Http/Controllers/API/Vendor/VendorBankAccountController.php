<?php

namespace App\Http\Controllers\API\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\VendorBankAccountStore;
use App\Http\Requests\API\Vendor\VendorBankAccountUpdate;
use App\Models\User;
use App\Models\VendorBankAccount;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VendorBankAccountController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $user = User::find(auth()->id());
            $bank_accounts = $user->vendor->vendor_bank_accounts;
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($bank_accounts);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VendorBankAccountStore $request)
    {
        try {
            DB::beginTransaction();
            $user = User::find(auth()->id());
            $bank_accounts = $user->vendor->vendor_bank_accounts()->create($request->validated());
            $bank_accounts->load('bank', 'currency');
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($bank_accounts);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $bank_account = VendorBankAccount::find($id);
            if (!$bank_account) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($bank_account->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $bank_account->load('bank', 'currency');
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($bank_account);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VendorBankAccountUpdate $request, string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $bank_account = VendorBankAccount::find($id);
            if (!$bank_account) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($bank_account->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $bank_account->update($request->validated());

            $bank_account->load('bank', 'currency');
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($bank_account);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $bank_account = VendorBankAccount::find($id);
            if (!$bank_account) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($bank_account->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $bank_account->update([
                'deleted_by' => $user->fullname ?? $user->username
            ]);
            $bank_account->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
