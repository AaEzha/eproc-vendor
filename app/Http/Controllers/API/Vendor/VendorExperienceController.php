<?php

namespace App\Http\Controllers\API\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\VendorExperienceStore;
use App\Http\Requests\API\Vendor\VendorExperienceUpdate;
use App\Models\User;
use App\Models\VendorExperience;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VendorExperienceController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $user = User::find(auth()->id());
            $experiences = $user->vendor->vendor_experiences;
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($experiences);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VendorExperienceStore $request)
    {
        try {
            DB::beginTransaction();
            $user = User::find(auth()->id());
            $experience = $user->vendor->vendor_experiences()->create($request->validated());
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($experience);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $experience = VendorExperience::find($id);
            if (!$experience) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($experience->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($experience);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VendorExperienceUpdate $request, string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $experience = VendorExperience::find($id);
            if (!$experience) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($experience->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $experience->update($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($experience);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $experience = VendorExperience::find($id);
            if (!$experience) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($experience->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $experience->update([
                'deleted_by' => auth()->id() ?? null,
            ]);
            $experience->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
