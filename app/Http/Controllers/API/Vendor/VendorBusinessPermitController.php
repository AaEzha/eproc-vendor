<?php

namespace App\Http\Controllers\API\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\VendorBusinessPermitStore;
use App\Http\Requests\API\Vendor\VendorBusinessPermitUpdate;
use App\Models\User;
use App\Models\VendorBusinessPermit;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VendorBusinessPermitController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $user = User::find(auth()->id());
            $business_permits = $user->vendor->vendor_business_permits()->orderBy('id')->get();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($business_permits);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VendorBusinessPermitStore $request)
    {
        try {
            DB::beginTransaction();
            $user = User::find(auth()->id());
            $business_permits = $user->vendor->vendor_business_permits()->create($request->all());
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($business_permits);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $business_permit = VendorBusinessPermit::find($id);
            if (!$business_permit) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($business_permit->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($business_permit);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VendorBusinessPermitUpdate $request, string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $business_permit = VendorBusinessPermit::find($id);
            if (!$business_permit) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($business_permit->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $business_permit->update($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($business_permit);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $business_permit = VendorBusinessPermit::find($id);
            if (!$business_permit) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($business_permit->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $business_permit->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
