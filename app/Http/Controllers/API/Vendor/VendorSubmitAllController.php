<?php

namespace App\Http\Controllers\API\Vendor;

use App\Enums\VendorStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\VendorSubmitAllStore;
use App\Models\User;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VendorSubmitAllController extends Controller
{
    use ApiResponder;
    /**
     * Handle the incoming request.
     */
    public function __invoke(VendorSubmitAllStore $request)
    {
        try {
            DB::beginTransaction();

            $user = User::find(auth()->id());
            $vendor = $user->vendor;
            $vendor->status = VendorStatus::NotVerified;
            $vendor->save();
            DB::commit();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($vendor);
    }
}
