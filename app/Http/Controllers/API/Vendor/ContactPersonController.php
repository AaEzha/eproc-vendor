<?php

namespace App\Http\Controllers\API\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\ContactPersonStore;
use App\Http\Requests\API\Vendor\ContactPersonUpdate;
use App\Models\User;
use App\Models\VendorContactPerson;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ContactPersonController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $user = User::find(auth()->id());
            $people = $user->vendor->contact_people;
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($people);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ContactPersonStore $request)
    {
        try {
            DB::beginTransaction();
            $user = User::find(auth()->id());
            $people = $user->vendor->contact_people()->create($request->validated());
            $people->load('vendor_position');
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($people);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $contact_person = VendorContactPerson::find($id);
            if (!$contact_person) return $this->error(__('auth.failed'),code: 404);

            // validasi kecocokan dgn vendor
            if ($contact_person->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $contact_person->load('vendor_position');
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($contact_person);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ContactPersonUpdate $request, string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $contact_person = VendorContactPerson::find($id);
            if (!$contact_person) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($contact_person->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $contact_person->update($request->validated());

            $contact_person->load('vendor_position');
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($contact_person);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $contact_person = VendorContactPerson::find($id);
            if (!$contact_person) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($contact_person->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $contact_person->update([
                'deleted_by' => auth()->id() ?? null,
            ]);
            $contact_person->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
