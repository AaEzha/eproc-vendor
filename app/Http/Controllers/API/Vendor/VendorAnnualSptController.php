<?php

namespace App\Http\Controllers\API\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\VendorAnnualSptStore;
use App\Http\Requests\API\Vendor\VendorAnnualSptUpdate;
use App\Models\User;
use App\Models\VendorAnnualSpt;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VendorAnnualSptController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $user = User::find(auth()->id());
            $annual_spts = $user->vendor->vendor_annual_spts;
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($annual_spts);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VendorAnnualSptStore $request)
    {
        try {
            DB::beginTransaction();
            $user = User::find(auth()->id());
            $annual_spts = $user->vendor->vendor_annual_spts()->create($request->all());
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($annual_spts);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $annual_spt = VendorAnnualSpt::find($id);
            if (!$annual_spt) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($annual_spt->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($annual_spt);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VendorAnnualSptUpdate $request, string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $annual_spt = VendorAnnualSpt::find($id);
            if (!$annual_spt) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($annual_spt->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $annual_spt->update($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($annual_spt);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $annual_spt = VendorAnnualSpt::find($id);
            if (!$annual_spt) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($annual_spt->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $annual_spt->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
