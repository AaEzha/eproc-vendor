<?php

namespace App\Http\Controllers\API\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\VendorExpertStore;
use App\Http\Requests\API\Vendor\VendorExpertUpdate;
use App\Models\User;
use App\Models\VendorExpert;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VendorExpertController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $user = User::find(auth()->id());
            $experts = $user->vendor->vendor_experts;
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($experts);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VendorExpertStore $request)
    {
        try {
            DB::beginTransaction();
            $user = User::find(auth()->id());
            $expert = $user->vendor->vendor_experts()->create($request->validated());
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($expert);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $expert = VendorExpert::find($id);
            if (!$expert) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($expert->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($expert);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VendorExpertUpdate $request, string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $expert = VendorExpert::find($id);
            if (!$expert) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($expert->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $expert->update($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($expert);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $user = User::find(auth()->id());
            $vendor = $user->vendor;

            $expert = VendorExpert::find($id);
            if (!$expert) return $this->error(__('auth.failed'), code: 404);

            // validasi kecocokan dgn vendor
            if ($expert->vendor_id != $vendor->getKey()) return $this->error(__('auth.failed'), code: 404);

            $expert->update([
                'deleted_by' => auth()->id() ?? null,
            ]);
            $expert->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
