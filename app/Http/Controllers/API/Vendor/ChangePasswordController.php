<?php

namespace App\Http\Controllers\API\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vendor\ChangePasswordRequest;
use App\Models\User;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ChangePasswordController extends Controller
{
    use ApiResponder;
    /**
     * Handle the incoming request.
     */
    public function __invoke(ChangePasswordRequest $request)
    {
        try {
            $password = $request->new_password;

            $user = User::find(auth()->id());
            $user->password = $password;
            $user->vendor->password = $password;
            $user->push();
        } catch (\Throwable $th) {
            Log::warning($th->getMessage());
            return $this->error($th->getMessage(), code: 401);
        }

        return $this->ok(message:__('passwords.reset'));
    }
}
