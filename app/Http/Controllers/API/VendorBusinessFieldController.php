<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\VendorBusinessFieldStore;
use App\Http\Requests\API\VendorBusinessFieldUpdate;
use App\Models\VendorBusinessField;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VendorBusinessFieldController extends Controller
{
    use ApiResponder;

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $vendor_business_fields = VendorBusinessField::oldest()->get();

        return $this->ok($vendor_business_fields);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VendorBusinessFieldStore $request)
    {
        try {
            $vendor_business_field = VendorBusinessField::create($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($vendor_business_field, code: 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(VendorBusinessField $vendor_business_field)
    {
        return $this->ok($vendor_business_field);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VendorBusinessFieldUpdate $request, VendorBusinessField $vendor_business_field)
    {
        try {
            $vendor_business_field->update($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($vendor_business_field);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(VendorBusinessField $vendor_business_field)
    {
        try {
            $vendor_business_field->update([
                'deleted_by' => auth()->id() ?? null,
            ]);
            $vendor_business_field->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
