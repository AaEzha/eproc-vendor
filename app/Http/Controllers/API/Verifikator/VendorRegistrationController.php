<?php

namespace App\Http\Controllers\API\Verifikator;

use App\Enums\VendorStatus;
use App\Http\Controllers\Controller;
use App\Models\Vendor;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VendorRegistrationController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            $query = Vendor::query()->select(['id', 'company_name', 'username', 'company_npwp', 'company_email', 'company_phone_number', 'vendor_type', 'status'])->where('status', VendorStatus::Registered);

            if ($request->has('company_name')) $query->where('company_name', 'like', '%' . $request->company_name . '%');
            if ($request->has('username')) $query->where('username', 'like', '%' . $request->username . '%');
            if ($request->has('company_npwp')) $query->where('company_npwp', 'like', '%' . $request->company_npwp . '%');
            if ($request->has('company_email')) $query->where('company_email', 'like', '%' . $request->company_email . '%');
            if ($request->has('company_phone_number')) $query->where('company_phone_number', 'like', '%' . $request->company_phone_number . '%');
            if ($request->has('vendor_type')) $query->where('vendor_type', 'like', '%' . $request->vendor_type . '%');

            $vendors = $query->get();

            $res = [];
            foreach ($vendors as $vendor) {
                # first Contact Person
                $contact_person_data = $vendor->contact_people()->first();
                $pic_name = $contact_person_data->contact_name;
                $pic_email = $contact_person_data->contact_email;
                $pic_npwp = $contact_person_data->contact_npwp;
                $pic_phone = $contact_person_data->contact_phone;
                $vendor = array_merge($vendor->toArray(), compact('pic_name', 'pic_email', 'pic_npwp', 'pic_phone'));
                $res[] = $vendor;
            }
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($res);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        return $this->error();
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return $this->error();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        return $this->error();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        return $this->error();
    }
}
