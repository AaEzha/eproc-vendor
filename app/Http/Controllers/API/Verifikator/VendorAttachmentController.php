<?php

namespace App\Http\Controllers\API\Verifikator;

use App\Enums\VendorStatus;
use App\Http\Controllers\Controller;
use App\Models\Vendor;
use App\Models\VendorAttachment;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VendorAttachmentController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return $this->error();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        return $this->error();
    }

    /**
     * Display the specified resource.
     */
    public function show(Vendor $vendor)
    {
        try {
            $attachments = $vendor->vendor_attachments;
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($attachments);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Vendor $vendor, VendorAttachment $vendor_attachment)
    {
        try {
            DB::beginTransaction();
            if ($vendor_attachment->vendor()->isNot($vendor)) throw new \Exception(__('auth.failed'));

            if ($vendor_attachment->is_verified == 0) {
                // terverifikasi
                $vendor_attachment->verified_by = auth()->user()->fullname;
                $vendor_attachment->verified_at = now();
                $vendor_attachment->is_verified = true;
            } else {
                // batal terverifikasi
                $vendor_attachment->verified_by = null;
                $vendor_attachment->verified_at = null;
                $vendor_attachment->is_verified = false;
            }
            $vendor_attachment->save();
            $vendor_attachment->refresh();

            // cek status keseluruhannya. jika semuanya verified, maka ubah status vendor menjadi Verified.
            $status = VendorStatus::Verified;
            foreach ($vendor->vendor_attachments as $attachment) {
                if ($attachment->is_verified == 0) {
                    $status = VendorStatus::NotVerified;
                    break;
                }
            }
            $vendor->status = $status;
            $vendor->save();

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($vendor_attachment);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Vendor $vendor)
    {
        return $this->error();
    }
}
