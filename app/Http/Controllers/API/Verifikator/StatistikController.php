<?php

namespace App\Http\Controllers\API\Verifikator;

use App\Enums\VendorStatus;
use App\Http\Controllers\Controller;
use App\Models\Vendor;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;

class StatistikController extends Controller
{
    use ApiResponder;
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        return $this->ok([
            'vendor_registered' => Vendor::where('status', VendorStatus::Registered)->count(),
            'vendor_verified' => Vendor::where('status', VendorStatus::Verified)->count(),
            'sangat_baik' => null,
            'baik' => null,
            'cukup' => null,
            'kurang' => null,
            'sangat kurang' => null,
        ]);
    }
}
