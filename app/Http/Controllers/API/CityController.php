<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\CityStore;
use App\Http\Requests\API\CityUpdate;
use App\Models\City;
use App\Models\Province;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CityController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $query = City::query()->orderBy('id');

        if ($request->has('province')) {
            $province = Province::find($request->province);
            if (!$province) throw new NotFoundHttpException('Province not found');
            $query->where('province_id', $request->province);
        }
        $cities = $query->get();
        // try {
        // } catch (\Throwable $th) {
        //     Log::error($th->getMessage());
        //     return $this->error($th->getMessage());
        // }

        return $this->ok($cities);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CityStore $request)
    {
        try {
            $cities = City::create($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($cities, code: 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(City $city)
    {
        return $this->ok($city);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CityUpdate $request, City $city)
    {
        try {
            $city->update($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($city);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(City $city)
    {
        try {
            $city->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
