<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\GroupMenu;
use App\Models\Menu;
use App\Models\User;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    use ApiResponder;
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $rootMenus = Menu::where('parent_id', 0)->with('children_menu')->get();

        $menus = $this->buildMenuStructure($rootMenus);

        return $this->ok($menus);
    }

    public function buildMenuStructure($menus, $level = 0)
    {
        $user = User::find(auth()->id());
        $result = [];
        foreach ($menus as $menu) {
            $children = $menu->children_menu ? $this->buildMenuStructure($menu->children_menu) : [];

            // data group_menus
            $group_menu = GroupMenu::where([
                'menu_id' => $menu->getKey(),
                'group_users_id' => $user->group_users_id
            ])->first();
            if (!$group_menu || !$group_menu->mview) continue;
            // end of data group_menus
            $result[] = [
                'menu_name' => $menu->menu_name,
                'urlto' => $menu->urlto,
                'keterangan' => $menu->keterangan,
                'access' => [
                    'view' => $group_menu->mview ? true : false,
                    'add' => $group_menu->madd ? true : false,
                    'edit' => $group_menu->medit ? true : false,
                    'approve' => $group_menu->mapprove ? true : false,
                    'ignore' => $group_menu->mignore ? true : false,
                    'export' => $group_menu->mexport ? true : false,
                ],
                // 'id' => $menu->menu_id,
                // 'parent_id' => $menu->parent_id,
                // 'level_menu' => $menu->level_menu,
                'children' => $children,
            ];
        }
        return $result;
    }
}
