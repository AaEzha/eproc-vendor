<?php

namespace App\Http\Controllers\API\Auth;

use App\Enums\VendorStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Auth\RegisterVendorStore;
use App\Mail\ConfirmRegistration;
use App\Mail\ErrorVendorCode;
use App\Models\City;
use App\Models\GroupUser;
use App\Models\User;
use App\Models\Vendor;
use App\Models\VendorContactPerson;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class RegisterVendorController extends Controller
{
    use ApiResponder;

    /**
     * Handle the incoming request.
     */
    public function __invoke(RegisterVendorStore $request)
    {
        DB::beginTransaction();

        try {
            $data = array_merge($request->validated(), [
                'vendor_number' => $this->getVendorNumber(),
                'status' => VendorStatus::Registered,
            ]);

            // validasi city dan province
            $city = City::where([
                'id' => $request->city_id,
                'province_id' => $request->province_id,
            ])->first();
            if (!$city) throw new \Exception('Provinsi dan Kota tidak selaras');

            // cek group user vendor
            $group_user = GroupUser::whereCode('vendor')->first();
            if (!$group_user) {
                Mail::to(env('EMAIL_ADMIN', 'reza@nurfachmi.com'))->queue(new ErrorVendorCode($request->getHost()));
                throw new \Exception('Error: Tidak ada vendor. Hubungi Administrator.');
            }

            $vendor = Vendor::create($data);

            $user = User::create([
                'email' => $data['company_email'],
                'user_name' => $data['username'],
                'password' => $data['password'],
                'group_users_id' => $group_user->getKey(),
            ]);

            // contact person
            $cp_names = explode("|", $data['contact_name']);
            $cp_emails = explode("|", $data['contact_email']);
            $cp_npwps = explode("|", $data['contact_npwp']);
            $cp_identity_nos = explode("|", $data['contact_identity_no']);
            $cp_phones = explode("|", $data['contact_phone']);
            $cp_position_ids = explode("|", $data['position_id']);

            foreach ($cp_names as $key => $cp_name) {
                if ($key === 0) {
                    // Pisahkan string menjadi array berdasarkan spasi
                    $nameParts = explode(' ', $cp_name);

                    // Nama depan adalah elemen pertama dari array
                    $firstname = array_shift($nameParts);
                    // Nama belakang adalah sisa dari elemen dalam array yang digabung kembali menjadi string
                    $lastname = implode(' ', $nameParts);

                    $user->firstname = $firstname;
                    $user->lastname = $lastname;
                    $user->phone = trim($cp_phones[$key]);
                    $user->company = $vendor->company_name;
                    $user->save();
                }
                $dataSaved = [
                    'contact_name' => trim($cp_name),
                    'contact_email' => trim($cp_emails[$key]),
                    'contact_npwp' => trim($cp_npwps[$key]),
                    'contact_identity_no' => trim($cp_identity_nos[$key]),
                    'contact_phone' => trim($cp_phones[$key]),
                    'position_id' => trim($cp_position_ids[$key]),
                    'vendor_id' => $vendor->getKey(),
                ];
                $contact_person[] = VendorContactPerson::create($dataSaved);
            }


            $res = compact('vendor', 'user', 'contact_person');
            DB::commit();
            $contact_people = VendorContactPerson::where('vendor_id', $vendor->getKey())->get();

            foreach ($contact_people as $person) {
                Mail::to($person->contact_email)->queue(new ConfirmRegistration($vendor, $person));
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($res, code: 201);
    }

    protected function getVendorNumber($prefix = "")
    {
        $latestVendor = Vendor::withTrashed()->latest('id')->first();
        if (is_null($latestVendor)) $numberVendors = 1;
        else $numberVendors = $latestVendor->id + 1;

        return $prefix . str($numberVendors)->padLeft(5, "0");
    }
}
