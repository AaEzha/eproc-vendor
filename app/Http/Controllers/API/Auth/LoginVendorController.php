<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Auth\LoginVendorRequest;
use App\Models\User;
use App\Models\Vendor;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class LoginVendorController extends Controller
{
    use ApiResponder;

    /**
     * Handle the incoming request.
     */
    public function __invoke(LoginVendorRequest $request)
    {
        try {
            $user = DB::table('users')
                ->join('vendors', 'users.user_name', '=', 'vendors.username')
                ->select('users.id as user_id', 'users.password', 'vendors.id as vendor_id', 'vendors.company_name')
                ->where('users.user_name', $request->username)
                ->where('vendors.is_active', 1)
                ->first();
            $admin = User::where('user_name', $request->username)->first();
            $vendor = Vendor::where('username', $request->username)->first();

            if (!$user || !Hash::check($request->password, $admin->password) || !Hash::check($request->password, $vendor->password)) throw new \Exception(__('auth.failed'));

            if (!$vendor->is_active) throw new \Exception(__('Status vendor belum terkonfirmasi.'));

            $res = [
                'user_id' => $admin->getKey(),
                'vendor_id' => $vendor->getKey(),
                'token' => $admin->createToken($admin->getKey())->plainTextToken
            ];
        } catch (\Throwable $th) {
            Log::warning($th->getMessage());
            return $this->error($th->getMessage(), code: 401);
        }

        return $this->ok($res);
    }
}
