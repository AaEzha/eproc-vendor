<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LogoutVendorController extends Controller
{
    use ApiResponder;
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        try {
            $request->user()->currentAccessToken()->delete();
        } catch (\Throwable $th) {
            Log::warning($th->getMessage());
            return $this->error($th->getMessage(), code: 401);
        }

        return $this->ok('Berhasil logout');
    }
}
