<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\ProvinceStore;
use App\Http\Requests\API\ProvinceUpdate;
use App\Models\Province;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProvinceController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $currencies = Province::orderBy('id')->get();

        return $this->ok($currencies);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProvinceStore $request)
    {
        try {
            $province = Province::create($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($province, code: 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Province $province)
    {
        return $this->ok($province);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProvinceUpdate $request, Province $province)
    {
        try {
            $province->update($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($province);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Province $province)
    {
        try {
            $province->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
