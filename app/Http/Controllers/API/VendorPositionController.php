<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\VendorPositionStore;
use App\Http\Requests\API\VendorPositionUpdate;
use App\Models\VendorPosition;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VendorPositionController extends Controller
{
    use ApiResponder;

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $vendor_positions = VendorPosition::oldest()->get();

        return $this->ok($vendor_positions);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VendorPositionStore $request)
    {
        try {
            $vendor_position = VendorPosition::create($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($vendor_position, code: 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(VendorPosition $vendor_position)
    {
        return $this->ok($vendor_position);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VendorPositionUpdate $request, VendorPosition $vendor_position)
    {
        try {
            $vendor_position->update($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($vendor_position);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(VendorPosition $vendor_position)
    {
        try {
            $vendor_position->update([
                'deleted_by' => auth()->id() ?? null,
            ]);
            $vendor_position->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
