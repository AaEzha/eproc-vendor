<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\CurrencyStore;
use App\Http\Requests\API\CurrencyUpdate;
use App\Models\Currency;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CurrencyController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $currencies = Currency::orderBy('name')->get();

        return $this->ok($currencies);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CurrencyStore $request)
    {
        try {
            $currency = Currency::create($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($currency, code: 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Currency $currency)
    {
        return $this->ok($currency);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CurrencyUpdate $request, Currency $currency)
    {
        try {
            $currency->update($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($currency);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Currency $currency)
    {
        try {
            $currency->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
