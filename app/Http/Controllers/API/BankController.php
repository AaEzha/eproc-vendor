<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\BankStore;
use App\Http\Requests\API\BankUpdate;
use App\Models\Bank;
use App\Traits\ApiResponder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BankController extends Controller
{
    use ApiResponder;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $banks = Bank::orderBy('bank_name')->get();

        return $this->ok($banks);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(BankStore $request)
    {
        try {
            $bank = Bank::create($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($bank, code: 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Bank $bank)
    {
        return $this->ok($bank);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(BankUpdate $request, Bank $bank)
    {
        try {
            $bank->update($request->validated());
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok($bank);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Bank $bank)
    {
        try {
            $bank->delete();
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->error($th->getMessage());
        }

        return $this->ok(code: 204);
    }
}
