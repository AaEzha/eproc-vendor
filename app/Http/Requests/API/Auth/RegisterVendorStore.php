<?php

namespace App\Http\Requests\API\Auth;

use App\Traits\CreatedBy;
use App\Traits\FailedValidation;
use Illuminate\Foundation\Http\FormRequest;

class RegisterVendorStore extends FormRequest
{
    use FailedValidation, CreatedBy;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            // authorization
            'username' => 'required|string|unique:vendors|unique:users,user_name|between:5,100',
            'password' => 'required|min:8',
            // company profile
            'company_name' => 'required|string|max:100',
            'company_npwp' => 'required|numeric|digits_between:15,16',
            'company_phone_number' => 'required|string|max:16',
            'company_email' => 'required|email|max:50|unique:vendors|unique:users,email',
            'company_fax' => 'required|string|max:16',
            'company_address' => 'required|string|max:200',
            'city_id' => 'required|exists:cities,id',
            'province_id' => 'required|exists:provinces,id',
            'postal_code' => 'required|numeric|max_digits:10',
            'vendor_type' => 'required|string|max:20',
            // contact person
            'contact_name' => 'required|string',
            'contact_email' => 'required|string',
            'contact_npwp' => 'required|string',
            'contact_identity_no' => 'required|string',
            'contact_phone' => 'required|string',
            'position_id' => 'required|string',
        ];
    }
}
