<?php

namespace App\Http\Requests\API\Vendor;

use App\Traits\FailedValidation;
use App\Traits\UpdateBy;
use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    use FailedValidation, UpdateBy;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'password' => 'required|current_password',
            'new_password' => 'required|string|min:8|confirmed',
        ];
    }

    public function attributes()
    {
        return [
            // 'password' => 'new password',
        ];
    }
}
