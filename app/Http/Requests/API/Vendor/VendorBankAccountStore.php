<?php

namespace App\Http\Requests\API\Vendor;

use App\Traits\CreatedBy;
use App\Traits\FailedValidation;
use Illuminate\Foundation\Http\FormRequest;

class VendorBankAccountStore extends FormRequest
{
    use FailedValidation, CreatedBy;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'bank_id' => 'required|exists:app_banks,id',
            'currency_id' => 'required|exists:currency,id',
            'account_number' => 'required|string|max:50',
        ];
    }
}
