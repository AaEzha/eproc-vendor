<?php

namespace App\Http\Requests\API\Vendor;

use App\Traits\FailedValidation;
use App\Traits\UpdateBy;
use Illuminate\Foundation\Http\FormRequest;

class ContactPersonUpdate extends FormRequest
{
    use FailedValidation, UpdateBy;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'contact_name' => 'required|string',
            'contact_email' => 'required|string',
            'contact_npwp' => 'required|string',
            'contact_identity_no' => 'required|string',
            'contact_phone' => 'required|string',
            'position_id' => 'required|string',
        ];
    }
}
