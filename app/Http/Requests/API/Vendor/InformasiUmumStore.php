<?php

namespace App\Http\Requests\API\Vendor;

use App\Traits\FailedValidation;
use App\Traits\UpdateBy;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class InformasiUmumStore extends FormRequest
{
    use FailedValidation, UpdateBy;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'company_name' => 'required|string|max:100',
            // 'company_npwp' => 'required|numeric|digits_between:15,16',
            'company_phone_number' => 'required|string|max:16',
            'company_email' => 'required|email|max:50',
            'company_fax' => 'required|string|max:16',
            'company_address' => 'required|string|max:200',
            'city_id' => 'required|exists:cities,id',
            'province_id' => 'required|exists:provinces,id',
            'postal_code' => 'required|numeric|max_digits:10',
            'vendor_type' => 'required|string|max:20',
        ];
    }
}
