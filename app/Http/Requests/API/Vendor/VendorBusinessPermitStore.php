<?php

namespace App\Http\Requests\API\Vendor;

use App\Traits\CreatedBy;
use App\Traits\FailedValidation;
use Illuminate\Foundation\Http\FormRequest;

class VendorBusinessPermitStore extends FormRequest
{
    use FailedValidation, CreatedBy;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'type' => 'required|string|exists:parameters,name',
            'permit_number' => 'required|string|max:150',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'licensing_agency' => 'required|string|max:150',
            'vendor_business_field_id' => 'required|numeric|exists:vendor_business_fields,id'
        ];
    }
}
