<?php

namespace App\Http\Requests\API\Vendor;

use App\Traits\FailedValidation;
use App\Traits\UpdateBy;
use Illuminate\Foundation\Http\FormRequest;

class VendorExpertUpdate extends FormRequest
{
    use FailedValidation, UpdateBy;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:100',
            'birth_date' => 'required|date',
            'identity_no' => 'required|string|size:16',
            'npwp_no' => 'required|string|size:16',
            'last_education' => 'required|string|max:100',
            'last_experience' => 'required|string|max:100',
        ];
    }
}
