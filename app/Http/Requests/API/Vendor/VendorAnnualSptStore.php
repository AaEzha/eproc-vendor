<?php

namespace App\Http\Requests\API\Vendor;

use App\Traits\CreatedBy;
use App\Traits\FailedValidation;
use Illuminate\Foundation\Http\FormRequest;

class VendorAnnualSptStore extends FormRequest
{
    use FailedValidation, CreatedBy;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'year' => 'required|numeric|digits:4',
            'spt_number' => 'required|string|max:20',
            'date' => 'required|date',
        ];
    }
}
