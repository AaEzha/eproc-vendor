<?php

namespace App\Http\Requests\API;

use App\Traits\CreatedBy;
use App\Traits\FailedValidation;
use Illuminate\Foundation\Http\FormRequest;

class CurrencyStore extends FormRequest
{
    use FailedValidation, CreatedBy;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'code' => 'required|string|max:20',
            'name' => 'required|string|max:20',
            'status' => 'required|boolean',
        ];
    }
}
