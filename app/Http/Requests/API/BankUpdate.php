<?php

namespace App\Http\Requests\API;

use App\Traits\FailedValidation;
use App\Traits\UpdateBy;
use Illuminate\Foundation\Http\FormRequest;

class BankUpdate extends FormRequest
{
    use FailedValidation, UpdateBy;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'bank_name' => 'required|string|max:100',
            'bank_logo' => 'nullable|image|max:512',
        ];
    }
}
