## Getting Started

Copy yourself a .env file

```bash
cp .env.example .env
```

Custom your database credential.

Install the dependencies.

```bash
composer install
```

Make sure you have the latest migration.

```bash
php artisan migrate
```

If this is your first time, then do this.

```bash
php artisan db:seed
```

Run local server

```bash
php artisan serve
```

Open [http://localhost:8000](http://localhost:8000) with your browser to see the result.

## Responses

| key     | data type    |
| ------- | ------------ |
| message | string       |
| data    | object/array |
| errors  | object/array |

#### Success response 👍

Status: `200` / `2xx`

```json
{
    "message": "Success", // rarely changes
    "data": {
        "key": "value"
    }
}
```

#### Error Response 👎

Status: `400` / `4xx` / `5xx`

```json
{
    "message": "Error", // often changing
    "data": null // rarely changes
}
```

#### Validation Error ❌

Status: `422`

```json
{
    "message": "Validation errors", // rarely changing
    "data": null, // rarely changes
    "errors": {
        "key": "value"
    }
}
```

## Learn More

To learn more about Laravel, take a look at the following resources:

-   [Laravel Documentation](https://laravel.com/docs) - learn about Laravel features and API.
-   [Learn Laravel](https://bootcamp.laravel.com) - an interactive Laravel tutorial.
