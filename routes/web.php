<?php

use App\Http\Controllers\ConfirmRegistrationController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return response()->json([
        'message' => 'Salam',
        'data' => null,
    ]);
});

Route::get('/verification/vendor/{vendor_number}/{vendor:vendor_number}/{random_code}', ConfirmRegistrationController::class)->name('confirm-registration');

Route::fallback(function () {
    return response()->json([
        'message' => 'Salam',
        'data' => null,
    ], 400);
});
