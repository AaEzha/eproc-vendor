<?php

use App\Http\Controllers\API\Auth\LoginVendorController;
use App\Http\Controllers\API\Auth\LogoutVendorController;
use App\Http\Controllers\API\Auth\RegisterVendorController;
use App\Http\Controllers\API\BankController;
use App\Http\Controllers\API\CityController;
use App\Http\Controllers\API\CurrencyController;
use App\Http\Controllers\API\MenuController;
use App\Http\Controllers\API\ParametersController;
use App\Http\Controllers\API\ProvinceController;
use App\Http\Controllers\API\Vendor\ChangePasswordController;
use App\Http\Controllers\API\Vendor\ContactPersonController;
use App\Http\Controllers\API\Vendor\InformasiUmumController;
use App\Http\Controllers\API\Vendor\VendorAnnualSptController;
use App\Http\Controllers\API\Vendor\VendorAttachmentController;
use App\Http\Controllers\API\Vendor\VendorBankAccountController;
use App\Http\Controllers\API\Vendor\VendorBusinessPermitController;
use App\Http\Controllers\API\Vendor\VendorDirectorController;
use App\Http\Controllers\API\Vendor\VendorExperienceController;
use App\Http\Controllers\API\Vendor\VendorExpertController;
use App\Http\Controllers\API\Vendor\VendorLegalFoundationController;
use App\Http\Controllers\API\Vendor\VendorSubmitAllController;
use App\Http\Controllers\API\VendorBusinessFieldController;
use App\Http\Controllers\API\VendorPositionController;
use App\Http\Controllers\API\Verifikator\StatistikController;
use App\Http\Controllers\API\Verifikator\VendorAttachmentController as VerifikatorVendorAttachmentController;
use App\Http\Controllers\API\Verifikator\VendorController;
use App\Http\Controllers\API\Verifikator\VendorRegistrationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('auth')->as('auth.')->middleware('guest')->group(function () {
    Route::post('register', RegisterVendorController::class)->name('register');
    Route::post('login', LoginVendorController::class)->name('login');
});

Route::prefix('auth')->as('auth.')->middleware('auth:sanctum')->group(function () {
    Route::post('logout', LogoutVendorController::class)->name('logout');
    Route::get('menu', MenuController::class)->name('menu');
});

Route::prefix('profile')->as('profile.')->middleware('auth:sanctum')->group(function () {
    Route::post('change-password', ChangePasswordController::class);
});

Route::prefix('master')->as('master.')->group(function () {
    Route::apiResource('parameters', ParametersController::class);
    Route::apiResource('vendor-position', VendorPositionController::class);
    Route::apiResource('vendor-business-field', VendorBusinessFieldController::class);
    Route::apiResource('bank', BankController::class);
    Route::apiResource('currency', CurrencyController::class);
    Route::apiResource('province', ProvinceController::class);
    Route::apiResource('city', CityController::class);
});

Route::middleware('auth:sanctum')->as('section.')->prefix('vendor')->group(function () {
    Route::apiResource('informasi-umum', InformasiUmumController::class);
    Route::apiResource('contact-person', ContactPersonController::class);
    Route::apiResource('bank', VendorBankAccountController::class);
    Route::apiResource('business-permit', VendorBusinessPermitController::class);
    Route::apiResource('legal-foundation', VendorLegalFoundationController::class);
    Route::apiResource('director', VendorDirectorController::class);
    Route::apiResource('annual-spt', VendorAnnualSptController::class);
    Route::apiResource('experience', VendorExperienceController::class);
    Route::apiResource('expert', VendorExpertController::class);
    Route::get('attachment/categories', [VendorAttachmentController::class, 'create']);
    Route::apiResource('attachment', VendorAttachmentController::class);
    Route::post('submit', VendorSubmitAllController::class);
});

Route::middleware('auth:sanctum')->as('verifikator.')->prefix('verifikator')->group(function () {
    Route::get('stats', StatistikController::class);
    Route::apiResource('vendor', VendorController::class);
    Route::get('vendor-attachments/{vendor}', [VerifikatorVendorAttachmentController::class, 'show']);
    Route::put('vendor-attachments/{vendor}/{vendor_attachment}', [VerifikatorVendorAttachmentController::class, 'update']);
    // Route::apiResource('vendor-attachments', VerifikatorVendorAttachmentController::class)->parameters([
    //     'vendor-attachments' => 'vendor'
    // ])->only('show');
    // Route::apiResource('verifikator', VendorRegistrationController::class);
});

Route::fallback(function () {
    return response()->json([
        'message' => 'Salam',
        'data' => null,
    ], 400);
});
