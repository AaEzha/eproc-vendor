<x-mail::message>
# Error Vendor Code

Host: {{ $host }}

Masalah: Tidak terdapat group user `vendor` dalam tabel `group_users` sehingga menyebabkan gagalnya registrasi vendor baru.

Solusi: Buat atau ubah record pada tabel `group_users` dengan value pada kolom code menjadi `vendor`.

Terima kasih.

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
