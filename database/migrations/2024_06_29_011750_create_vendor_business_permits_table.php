<?php

use App\Models\Vendor;
use App\Models\VendorBusinessField;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vendor_business_permits', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Vendor::class)->nullable();
            $table->string('type', 100)->nullable()->comment('jenis izin dari table parameters grup Izin Usaha');
            $table->string('permit_number', 100)->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('licensing_agency', 100)->nullable();
            $table->foreignIdFor(VendorBusinessField::class)->nullable();
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table->string('deleted_by', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vendor_business_permits');
    }
};
