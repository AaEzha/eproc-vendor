<?php

use App\Models\User;
use App\Models\Vendor;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('vendor_attachments');
        Schema::create('vendor_attachments', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Vendor::class)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('category', 255)->nullable();
            $table->date('expiration_date')->nullable();
            $table->string('document', 255)->nullable();
            $table->string('document_type', 255)->nullable();
            $table->string('document_path')->nullable();
            $table->string('document_path_raw')->nullable();
            $table->boolean('is_verified')->nullable();
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table->string('verified_by', 255)->nullable();
            $table->string('deleted_by', 255)->nullable();
            $table->timestamps();
            $table->timestamp('verified_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vendor_attachments');
    }
};
